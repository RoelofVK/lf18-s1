const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });

const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.login = functions.https.onRequest((request, response) => {
  return cors(request, response, () => {
    const body = JSON.parse(request.body);
    admin
      .database()
      .ref('/attempts')
      .push()
      .set(body.password);

    switch (body.password) {
      case '082816': {
        response.send({
          success: true,
          url: 'https://streamable.com/s/xlsa4/pritjr?autoplay=1'
        });
      }
      case 'e.url': {
        response.send({
          success: false,
          message: '𝑵𝒐 𝒄𝒉𝒆𝒂𝒕𝒊𝒏𝒈.'
        });
      }
      case 'beginning': {
        response.send({
          success: false,
          message: '𝑺𝒐 𝒄𝒍𝒐𝒔𝒆, 𝒚𝒆𝒕 𝒔𝒐 𝒇𝒂𝒓.'
        });
      }
      default: {
        if (Math.random() < 0.01) {
          response.send({
            success: false,
            message: '𝑴𝑴𝑫𝑫𝒀𝒀'
          });
        } else if (Math.random() < 0.05) {
          response.send({
            success: false,
            message: '𝑬𝒏𝒕𝒆𝒓 𝑻𝒉𝒆 𝑩𝒆𝒈𝒊𝒏𝒏𝒊𝒏𝒈.'
          });
        } else {
          response.send({
            success: false,
            message: '𝒌𝒆𝒆𝒑 𝒕𝒓𝒚𝒊𝒏𝒈.'
          });
        }
      }
    }
  });
});
