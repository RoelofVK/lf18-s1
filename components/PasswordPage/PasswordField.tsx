import React from 'react';
import { Formik } from 'formik';
import fetch from 'isomorphic-unfetch';

import Video from './VideoEmbed';

export default class PasswordField extends React.Component {
  state = {
    showVideo: false,
    videoUrl: ''
  };

  public handleSubmit = (
    values: any,
    { setSubmitting, setErrors, resetForm }
  ) => {
    console.log('𝑷𝑨𝑺𝑺𝑾𝑶𝑹𝑫 𝑬𝑵𝑻𝑬𝑹𝑬𝑫:', values.password);
    fetch('https://us-central1-prd-lf18.cloudfunctions.net/login', {
      method: 'POST',
      body: JSON.stringify(values)
    })
      .then(r => r.json())
      .then(data => {
        console.log(data);
        if (!data.success) {
          resetForm();
          setErrors({
            password: data.message
            // password: 'Not yet'
          });
          return;
        }

        this.setState({
          showVideo: true,
          videoUrl: data.url
        });
      });
  };

  render() {
    return (
      <div>
        <Formik
          initialValues={{ password: '' }}
          onSubmit={this.handleSubmit.bind(this)}
          render={({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            errors
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="passwordfield__wrapper">
                <input
                  type="password"
                  name="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  className={`passwordfield__input ${
                    errors.password ? 'hasError' : ''
                  }`}
                  // placeholder="𝓔𝓷𝓽𝓮𝓻 𝓽𝓱𝓮 𝓹𝓪𝓼𝓼𝔀𝓸𝓻𝓭"
                />
                {errors.password && (
                  <div className="passwordfield__error">{errors.password}</div>
                )}
              </div>
              {this.state.showVideo && <Video url={this.state.videoUrl} />}
            </form>
          )}
        />

        <style jsx>{`
          :global(body) {
            margin: 0;
            font-family: 'VT323', monospace;
          }

          .passwordfield__wrapper {
            width: 100%;
            position: relative;
          }

          .passwordfield__input {
            font-family: 'Roboto mono', monospace;
            box-sizing: border-box;
            position: relative;
            background: white;
            border: 2px solid white;
            padding: 0.6em;
            color: white;
            width: 100%;
            font-size: 32px;
            box-sizing: border-box;
            color: black;

            box-shadow: 0 0 500em 2em rgba(255, 255, 255, 0.05);
            transition: box-shadow 0.3s ease;
          }

          .passwordfield__error {
            font-family: 'Roboto mono', monospace;
            position: absolute;
            top: 0;
            background: black;
            border: 2px solid red;
            padding: 0.6em;
            color: red;
            width: 100%;
            font-size: 32px;
            box-sizing: border-box;
            text-align: center;

            opacity: 1;
            animation: flickerIn 0.2s linear, flickerOut 0.2s linear 2s forwards;
          }

          .passwordfield__error .hide {
            opacity: 0;
          }

          @keyframes flickerIn {
            0% {
              opacity: 0;
            }
            10% {
              opacity: 1;
            }
            15% {
              opacity: 0;
            }
            25% {
              opacity: 1;
            }
            35% {
              opacity: 0;
            }
            40% {
              opacity: 1;
            }
            100% {
              opacity: 1;
            }
          }

          @keyframes flickerOut {
            0% {
              opacity: 0;
            }
            10% {
              opacity: 1;
            }
            15% {
              opacity: 0;
            }
            25% {
              opacity: 1;
            }
            35% {
              opacity: 0;
            }

            100% {
              opacity: 0;
            }
          }

          .hasError {
          }

          .passwordfield__input:placeholder {
          }

          // .passwordfield__input:after {
          //   position: absolute;
          //   content: ' ';
          //   bottom: 0;
          //   left: 0;
          //   width: 100%;
          //   height: 100%;
          //   background: white;
          //   z-index: 15;
          // }

          .passwordfield__input:focus {
            outline: none;
            box-shadow: 0 0 12000em 2.2em rgba(255, 255, 255, 0.06);
          }
        `}</style>
      </div>
    );
  }
}
