const MonitorEffect = () => (
  <div>
    <div className="monitor-effect__wrapper">
      <div className="monitor-effect__scanlines cover" />
      <div className="monitor-effect__glow cover" />
    </div>
    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: 'VT323', monospace;
      }

      .cover {
        position: fixed;
        height: 100%;
        left: 0;
        top: 0;
        width: 100%;
        pointer-events: none;
        z-index: 15;
      }

      .monitor-effect__scanlines {
        background: linear-gradient(
          to bottom,
          rgba(255, 255, 255, 0),
          rgba(255, 255, 255, 0) 50%,
          rgba(0, 0, 0, 0.2) 70%,
          rgba(0, 0, 0, 0.6)
        );
        background-size: 100% 0.3rem;
        border-radius: 2rem;
        position: absolute;
      }

      .monitor-effect__glow {
        animation: crt-glow 60s infinite;
        background: radial-gradient(
          circle at center,
          rgba(200, 200, 200, 1) 0%,
          rgba(200, 200, 200, 0.88) 58%,
          rgba(200, 200, 200, 0.57) 80%,
          rgba(200, 200, 200, 0.27) 93%,
          rgba(200, 200, 200, 0) 100%
        );

        opacity: 0.004;
      }

      @keyframes crt-glow {
        0% {
          opacity: 0.04;
        }
        50% {
          opacity: 0.08;
        }
      }
    `}</style>
  </div>
);

export default MonitorEffect;
