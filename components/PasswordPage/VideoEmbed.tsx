import React from 'react';
import ReactDOM from 'react-dom';

export default class PasswordField extends React.Component {
  render() {
    return ReactDOM.createPortal(
      <React.Fragment>
        <div className="video__wrapper">
          {this.props.url !== '' && (
            <iframe
              src={this.props.url}
              frameBorder="0"
              width="100%"
              height="100%"
              allowFullScreen
              style={{ width: '100%', height: '100%', position: 'absolute' }}
            />
          )}
        </div>
        <style jsx>{`
          :global(body) {
            margin: 0;
            font-family: 'VT323', monospace;
          }

          .video__wrapper {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: black;
          }
        `}</style>
      </React.Fragment>,
      document.body
    );
  }
}
