import React from 'react';

const Logo = () => (
  <div className="logo__wrapper">
    <img
      src="./static/LF18.svg"
      alt=""
      width="100%"
      height="100%"
      className="logo__img"
    />
    <style jsx>{`
      .logo__wrapper {
        position: relative;
      }

      .logo__wrapper:before {
        content: ' ';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: url('./static/LF18.svg');
        opacity: 0.5;
        background-position: center;
        background-size: contain;
        animation: glitchOne 2s linear infinite;
        z-index: -2;
      }

      .logo__wrapper:after {
        content: ' ';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: url('./static/LF18.svg');
        opacity: 0.5;
        background-position: center;
        background-size: contain;
        animation: glitchTwo 2s linear infinite;
        z-index: -2;
      }

      @keyframes glitchOne {
        0% {
          top: 0;
          filter: brightness(40%) sepia(100%) hue-rotate(240deg) contrast(15)
            saturate(30%);
        }
        2% {
          top: -5px;
        }
        4% {
          top: 5px;
        }
        6% {
          top: -15px;
          left: 25px;
        }
        7.5% {
          top: 10px;
          right: -5px;
        }
        8% {
          top: 5px;
          left: 30px;
          opacity: 0.5;
        }
        10% {
          top: 0;
          left: 0;
          opacity: 0;
        }
        100% {
          top: 0;
          left: 0;
          opacity: 0;
          filter: brightness(50%) sepia(100%) hue-rotate(240deg) contrast(15)
            saturate(30%);
        }
      }
      @keyframes glitchTwo {
        0% {
          top: 0;
          filter: brightness(40%) sepia(100%) hue-rotate(218deg) contrast(15);
        }
        2% {
          top: 8px;
        }
        3% {
          top: -10px;
        }
        5.5% {
          top: 15px;
          left: -30px;
        }
        8.3% {
          top: -15px;
          left: -20px;
          opacity: 0.5;
        }
        12% {
          top: 0;
          left: 0;
          opacity: 0;
        }
        100% {
          top: 0;
          left: 0;
          opacity: 0;
          filter: brightness(40%) sepia(100%) hue-rotate(218deg) contrast(15);
        }
      }
    `}</style>
  </div>
);

export default Logo;
