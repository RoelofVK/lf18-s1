const withTypescript = require('@zeit/next-typescript');

const config = {
  webpack: (config, { buildId, dev }) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    };

    return config;
  }
};

module.exports = withTypescript(config);
